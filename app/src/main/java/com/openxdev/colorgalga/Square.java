package com.openxdev.colorgalga;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;

public class Square {

    private int xPosition;
    private int yPosition;
    private int width;
    Bitmap bulletImage;
    // make the hitbox
    Rect hitbox;

    public Square(Context context, int x, int y) {
        this.bulletImage = BitmapFactory.decodeResource(context.getResources(), R.drawable.download);

        this.xPosition = x;
        this.yPosition = y;
        this.width = width;

        this.hitbox = new Rect(
                this.xPosition,
                this.yPosition,
                this.xPosition + this.bulletImage.getWidth(),
                this.yPosition + this.bulletImage.getHeight()
        );

    }



    public Rect getHitbox() {
        return hitbox;
    }

    public void setHitbox(Rect hitbox) {
        this.hitbox = hitbox;
    }

    public void updateHitbox() {
        this.hitbox.left = this.xPosition;
        this.hitbox.top = this.yPosition;
        this.hitbox.right = this.xPosition + this.width;
        this.hitbox.bottom = this.yPosition + this.width;
    }

    public Bitmap getBitmap() {
        return this.bulletImage;
    }
    public int getXPosition() {
        return this.xPosition;
    }
    public int getYPosition() {
        return this.yPosition;
    }


    public void setXPosition(int xPosition) {
        this.xPosition = xPosition;
        this.updateHitbox();
    }



    public void setYPosition(int yPosition) {
        this.yPosition = yPosition;
        this.updateHitbox();
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }
}

