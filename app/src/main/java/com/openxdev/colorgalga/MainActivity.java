package com.openxdev.colorgalga;

import android.graphics.Point;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Display;
import android.view.Window;
import android.view.WindowManager;


public class MainActivity extends AppCompatActivity {

    GameEngine colorGalga;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        colorGalga = new GameEngine(this, size.x, size.y);
        setContentView(colorGalga);
    }

    @Override
    protected void onResume() {
        super.onResume();
        colorGalga.startGame();
    }

    @Override
    protected void onPause() {
        super.onPause();
        colorGalga.pauseGame();
    }
}
