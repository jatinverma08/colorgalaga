package com.openxdev.colorgalga;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.util.Log;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

public class GameEngine extends SurfaceView implements Runnable {

    // Android debug variables
    final static String TAG="COLORR GALAGA";

    // screen size
    int screenHeight;
    int screenWidth;
    int i;

    // game state
    boolean gameIsRunning;

    // threading
    Thread gameThread;


    Player player;
    Enemy enemy;
    Enemy enemy1 [] = new Enemy[10];
    ArrayList <Enemy> enemy2 = new ArrayList<Enemy>();

    // drawing variables
    SurfaceHolder holder;
    Canvas canvas;
    Paint paintbrush;
    Bitmap bgStars;
    int bgStarsX = 0;
    int bgStarsXRight;

    Bitmap bgDust;
    int bgDustX;
    int bgDustXRight;

    int VISIBLE_RIGHT;
    int VISIBLE_LEFT;
    int VISIBLE_TOP;
    int VISIBLE_BOTTOM;


    final int PLAYER_DISTANCE = 100;
    final int bullet_DISTANCE = 100;
    // SPRITES
    Square bullet;


    int SQUARE_WIDTH = 5;


    public GameEngine(Context context, int w, int h) {
        super(context);

        this.holder = this.getHolder();
        this.paintbrush = new Paint();

        this.screenWidth = w;
        this.screenHeight = h;


        // setup visible game play area variables
        this.VISIBLE_LEFT = 20;
        this.VISIBLE_TOP = 10;
        this.VISIBLE_RIGHT = this.screenWidth - 20;
        this.VISIBLE_BOTTOM = (int) (this.screenHeight * 0.8);


        this.printScreenInfo();

        this.spawnPlayer();

        this.spawnEnemy();


        // initalize sprites
        bullet = new Square(this.getContext(),this.screenWidth/2 - 120, this.screenHeight - 270);
        // @TODO: Any other game setup
//
//        this.playerPos = new Point();
//        this.playerPos.x = 100;
//        this.playerPos.y = 120;

        //this.playerImage = BitmapFactory.decodeResource(context.getResources(),
                //R.drawable.player_ship);


        this.setupBackground();

        //setuo the initial position of player


        // setup the hitbox
        //this.playerHitbox = new Rect(100,120,100+this.playerImage.getWidth(),120+this.playerImage.getHeight());



    }

    private void spawnPlayer() {
        // put player in middle of screen --> you may have to adjust the Y position
        // depending on your device / emulator
        player = new Player(this.getContext(), this.screenWidth/2 - 150, this.screenHeight - 200);

    }

    private void spawnEnemy() {
        // put player in middle of screen --> you may have to adjust the Y position
        // depending on your device / emulator


        // Adding multiple enemies to arraylist
        Random rand = new Random();

        for (i = 0; i<10; i++) {
            int xx = rand.nextInt(this.screenWidth-100);
            int yy = rand.nextInt(this.screenHeight/2 - 150);
            enemy = new Enemy(this.getContext(), xx, yy);
            enemy2.add(enemy);
            Log.d("enemy","added");
        }

        //array of enemy ends here
    }


    private void printScreenInfo() {

        Log.d(TAG, "Screen (w, h) = " + this.screenWidth + "," + this.screenHeight);
    }


    private void setupBackground() {
        // get the background images from `res` folder and
        // scale it to be the same size as the phone
        this.bgStars = BitmapFactory.decodeResource(this.getResources(), R.drawable.bg_stars);
        this.bgStars = Bitmap.createScaledBitmap(this.bgStars, this.screenWidth, this.screenHeight, false);

        this.bgDust = BitmapFactory.decodeResource(this.getResources(), R.drawable.bg_dust);
        this.bgDust = Bitmap.createScaledBitmap(this.bgDust, this.screenWidth, this.screenHeight, false);
    }


    private void updateBackgroundPosition() {

        // Move the stars background
        this.bgStarsX = this.bgStarsX - 25;
        this.bgStarsXRight = this.bgStars.getWidth() - (-this.bgStarsX);

        if (bgStarsXRight <= 0) {
            this.bgStarsX = 0;
        }

        // Move the dust background -- it should move at a different speed
        this.bgDustX = this.bgDustX - 10;
        this.bgDustXRight = this.bgDust.getWidth() - (-this.bgDustX);

        if (bgDustXRight <= 0) {
            this.bgDustX = 0;
        }
    }

    @Override
    public void run() {
        while (gameIsRunning == true) {
            this.updatePositions();
            this.redrawSprites();
            //this.setFPS();
        }
    }
    public void startGame() {
        gameIsRunning = true;
        gameThread = new Thread(this);
        gameThread.start();
    }

    public void pauseGame() {
        gameIsRunning = false;
        try {
            gameThread.join();
        } catch (InterruptedException e) {
            // Error
        }
    }

    boolean movingRight = true;
    public void updatePositions() {
        //@TODO: Update background
        this.updateBackgroundPosition();

        player.updatePlayerPosition();

        //----------------------
        // COLLISION DETECTION
        //----------------------
        // if the player moves to the right and hits
        // the wallthen it will move to the opposite direction
        // and same as the left side

        //@TODO Not working right now


// 1. calculate distance between bullet and enemy
        double a = this.enemy.getXPosition() - this.bullet.getXPosition()+10;
        double b = this.enemy.getYPosition() - this.bullet.getYPosition()+10;

        // d = sqrt(a^2 + b^2)

        double d = Math.sqrt((a * a) + (b * b));

        Log.d(TAG, "Distance to enemy: " + d);

        // 2. calculate xn and yn constants
        // (amount of x to move, amount of y to move)
        double xn = (a / d);
        double yn = (b / d);

        // 3. calculate new (x,y) coordinates
        int newX = this.bullet.getXPosition() + (int) (xn * 20);
        int newY = this.bullet.getYPosition() + (int) (yn * 20);
        this.bullet.setXPosition(newX);
        this.bullet.setYPosition(newY);

        Log.d(TAG,"----------");

        if (bullet.getHitbox().intersect(enemy.getHitbox())) {



            // RESTART THE BULLET FROM INITIAL POSITION
            this.bullet.setXPosition(this.player.getXPosition());
            this.bullet.setYPosition(this.player.getYPosition());

            // RESTART THE HITBOX
            this.bullet.updateHitbox();


        }


    }


    public void redrawSprites() {
        if (holder.getSurface().isValid()) {

            // initialize the canvas
            canvas = holder.lockCanvas();
            // --------------------------------


            //====================== CODE FOR BACKGROUNDS =======================//
            canvas.drawBitmap(this.bgStars, this.bgStarsX, 0, null);
            canvas.drawBitmap(this.bgStars, this.bgStarsXRight, 0, null);

            // Draw the dust
            canvas.drawBitmap(this.bgDust, this.bgDustX, 0, null);
            canvas.drawBitmap(this.bgDust, this.bgDustXRight, 0, null);


            //====================== END CODE FOR BACKGROUNDS =======================//

            //-----------------------
            //      PLAYER  START
            //-----------------------

            // Draw the player
            //canvas.drawBitmap(this.player.getBitmap(), this.screenWidth/2 -150, this.screenHeight-200, paintbrush);
            canvas.drawBitmap(this.player.getBitmap(), this.player.getXPosition(), this.player.getYPosition(), paintbrush);


            //-----------------------
            //      ENEMY  START
            //-----------------------

            // Draw the enemy
            //canvas.drawBitmap(this.enemy.getBitmap(), this.screenWidth/2 -150, this.screenHeight-1200, paintbrush);
            //canvas.drawBitmap(this.enemy.getBitmap(), this.screenWidth/2 -150, this.screenHeight-1300, paintbrush);
            //canvas.drawBitmap(this.enemy.getBitmap(), this.enemy.getXPosition(), this.enemy.getYPosition(), paintbrush);

//            canvas.drawBitmap(this.enemy1[i].getBitmap(), this.enemy1[i].getXPosition(), this.enemy1[i].getYPosition(), paintbrush);

            for (Enemy temp : enemy2) {
                canvas.drawBitmap(temp.getBitmap(), temp.getXPosition(), temp.getYPosition(), paintbrush);

            }



            // draw bullet

            canvas.drawBitmap(
                    this.bullet.getBitmap(),
                    this.bullet.getXPosition(),

                    this.bullet.getYPosition() ,
                    paintbrush
            );

            // draw the bullet hitbox
            paintbrush.setColor(Color.RED);
            paintbrush.setStyle(Paint.Style.STROKE);
            canvas.drawRect(
                    this.bullet.getHitbox(),
                    paintbrush
            );

            // --------------------------------
            holder.unlockCanvasAndPost(canvas);

        }

    }

    public void setFPS() {
        try {
            gameThread.sleep(120);
        }
        catch (Exception e) {

        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int userAction = event.getActionMasked();
        //@TODO: What should happen when person touches the screen?
        if (userAction == MotionEvent.ACTION_DOWN) {
            // user pushed down on screen

            Log.d(TAG, "The person tapped: (" + event.getX() + "," + event.getY() + ")");


            this.bullet.setYPosition(this.bullet.getYPosition()-bullet_DISTANCE);

            // update hitbox position
            this.bullet.getHitbox().left = this.bullet.getXPosition();
            this.bullet.getHitbox().top = this.bullet.getYPosition();
            this.bullet.getHitbox().right = this.bullet.getXPosition() + this.bullet.getWidth();
            this.bullet.getHitbox().bottom = this.bullet.getYPosition() ;



            if (event.getX() < this.screenWidth / 2) {
                Log.d(TAG, "Person clicked LEFT side");
                this.player.setXPosition(this.player.getXPosition() - PLAYER_DISTANCE);
               //player.updatePlayerPosition();

                if ((this.player.getXPosition() > this.VISIBLE_RIGHT)) {
                    Log.d(TAG, "Racket reached right of screen. Changing direction!");
                    movingRight = true;
                }

                if ((this.player.getXPosition() < this.VISIBLE_LEFT)) {
                    Log.d(TAG, "Racket reached left of screen. Changing direction!");
                    movingRight = false;
                }
            }
            else {
                Log.d(TAG, "Person clicked RIGHT side");
                this.player.setXPosition(this.player.getXPosition() + PLAYER_DISTANCE);

            }




        }
        else if (userAction == MotionEvent.ACTION_UP) {
        }

        return true;
    }
}
